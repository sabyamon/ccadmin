CCAdmin
=======

This will hold code related to CCadmin POC

***** Config node to store the index values for each website node, group name and template name and alert message node name .


Manage Website ::

1. Create Website -- Done
2. Delete Website -- Done | 23rd August
3. Get All Websites -- Done 
4. Upon deletion of a website , delete website from all groups -- Done  || Same method will be used for Manage Group Item 6 . | 23rd August
5. Change the way website node name is created | read value from website config node and increment it.  -- Done



Manage Groups ::

1. Create Group -- Done 
2. Delete Group -- Done | 23rd August
3. Get All Groups -- Done 
4. Upon creation of a group, add the group name to the website associated with this group -- Done | 23rd August
5. Upon deletion of a group , delete group name from all websites -- Done | 23rd August
6. Delete Websites from Group -- TBD || Need to reuse Manage Website Step 4.
7. Update Group -- TBD
8. Change the way group node name is created | read value from group config node and increment it.  -- Done


Manage Template ::

1. Create Template -- Done
2. Delete Template -- Done
3. Get All Template -- Done


Manage Alert Message ::

1. Create Alert Message -- In Progress
2. Delete Alert Message -- TBD
3. Get All Alert Messages -- TBD
4. Create Page and Save it under ccmessages -- In Progress
5. Apply CSS by default and alert image by default -- TBD 
6. Activate the page once , author clicks on Activate button. -- TBD
7. Upon Deletion of a website , the alert message and the ccpage needs to be deleted -- TBD


Deactivation of an alert message ::

1. When an alert message is deactivated , goto the ccpage of the corresponding websites , and remove the message and activate the page.



