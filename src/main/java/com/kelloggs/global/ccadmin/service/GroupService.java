package com.kelloggs.global.ccadmin.service;

import java.util.List;
import javax.jcr.Session;

import com.kelloggs.global.ccadmin.vo.FilterVO;
import com.kelloggs.global.ccadmin.vo.GroupVO;


public interface GroupService {

	/**
	 * Adds a group to CRX , given a groupName , List of website paths and names.
	 * @param websiteURL
	 * @param websitePaths
	 * @param websiteNames
	 * @param currentSession
	 * @return
	 */
	public String addGroup(String groupName , List<String> websiteNodeNames , Session currentSession);
	
	/**
	 * Lists all the groups created by admin . 
	 * @param currentSession
	 * @return
	 */
	public List<GroupVO> getAllGroups(Session currentSession);
	
	/**
	 * Delete a group given its name.
	 * The method will also delete the references of the deleted website in the webGroups and alerts
	 * @param websiteNodes
	 */
	public void deleteGroup(List<String> groupNames , Session session);
	
	/**
	 * Delete websites from group given their group name , website name and website path. 
	 * @param websiteName
	 * @param websitePath
	 */
	public void deleteWebsiteFromGroup(String groupName , String websiteName,String websitePath, Session session);
	
	/**
	 * Return a list of groups that have been filtered according to the criteria
	 * listed in the filterVO
	 */
	public List<GroupVO> getGroups(FilterVO filterVO);
	
}


