package com.kelloggs.global.ccadmin.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Replicator;
import com.kelloggs.global.ccadmin.constant.CCAdminConstant;
import com.kelloggs.global.ccadmin.constant.WebsiteEnum;
import com.kelloggs.global.ccadmin.service.WebsiteService;
import com.kelloggs.global.ccadmin.util.CCAdminUtil;
import com.kelloggs.global.ccadmin.vo.FilterVO;
import com.kelloggs.global.ccadmin.vo.WebsiteVO;

public class WebsiteServiceImpl implements WebsiteService {

	private static final Logger log = LoggerFactory.getLogger(WebsiteServiceImpl.class);

	public WebsiteServiceImpl(SlingHttpServletRequest slingRequest, Replicator pReplicator) {
		// Will be in use if we need to replicate the websites to publish .
	}

	public WebsiteServiceImpl() {

	}

	/**
	 * This method will add website to the list of websites and will replicate
	 * it.
	 */
	@Override
	public WebsiteVO addWebsite(String websiteURL, String locale, Session currentSession) {
		log.info("Inside ManageWebsiteImpl : addWebsite method");
		
		CCAdminUtil ccAdminUtil = new CCAdminUtil() ;
		String nextValue = ccAdminUtil.getNextValueForNode(WebsiteEnum.WEBSITES_NODE.toString(), currentSession);
		String websiteNodeName = CCAdminConstant.WEBSITE_ID_PREFIX + nextValue;
		WebsiteVO websiteVO = new WebsiteVO();
		websiteVO.setWebsiteURL(websiteURL);
		websiteVO.setLocale(locale);
		websiteVO.setWebsiteID(websiteNodeName);
		String status = createWebsiteInCRX(websiteVO, currentSession);

		return websiteVO;

	}

	/**
	 * This method will create website nodes in CRX. Code will replace all "."
	 * to "_" . url+locale combination will be unique.
	 * 
	 * @param websiteVO
	 */
	private String createWebsiteInCRX(WebsiteVO websiteVO, Session currentSession) {
		
		log.info("Creating website node in createWebsiteInCRX()");
		try {

			if (currentSession.nodeExists(WebsiteEnum.WEBSITES_NODE.toString())) {
				Node websiteRootNode = currentSession.getNode(WebsiteEnum.WEBSITES_NODE.toString());
				String websiteNodeName = websiteVO.getWebsiteID() ;
				if (currentSession.nodeExists(websiteRootNode.getPath() + "/" + websiteNodeName)) {
					return "Website Exists";
				} else {
					Node currentWebsiteNode = websiteRootNode.addNode(websiteNodeName);
					currentWebsiteNode.setProperty(CCAdminConstant.WEBSITE_ID, websiteNodeName);
					currentWebsiteNode.setProperty(CCAdminConstant.WEBSITE_URL, websiteVO.getWebsiteURL());
					currentWebsiteNode.setProperty(CCAdminConstant.WEBSITE_LOCALE, websiteVO.getLocale());
					// set empty placeholders for these properties on creation
					currentWebsiteNode.setProperty(CCAdminConstant.WEBSITE_ASSOC_GROUP, new String[] {});
					currentWebsiteNode.setProperty(CCAdminConstant.WEBSITE_ASSOC_ALERTS, new String[] {});
					currentSession.save();
					currentSession.logout();

					return ("Create website " + websiteNodeName);

				}
			} else {

				return "Website Config Node Does not exist";
			}

		} catch (PathNotFoundException e) {
			log.error("No Path", e);
		} catch (RepositoryException e) {
			log.error("Repository problem", e);
		} finally {
			currentSession.logout();
		}
		return "";

	}

	/**
	 * This method will get all the websites created before and send it back to
	 * the caller client.
	 */
	@Override
	public List<WebsiteVO> getAllWebsites(Session currentSession) {
		try {
			if (currentSession.nodeExists(WebsiteEnum.WEBSITES_NODE.toString())) {

				Node websiteRootNode = currentSession.getNode(WebsiteEnum.WEBSITES_NODE.toString());
				List<WebsiteVO> websiteVOList = new ArrayList<WebsiteVO>();
				NodeIterator ndItr = websiteRootNode.getNodes();
				while (ndItr.hasNext()) {

					WebsiteVO webVO = new WebsiteVO();
					Node websiteNode = ndItr.nextNode();

					webVO.setLocale(websiteNode.getProperty(CCAdminConstant.WEBSITE_LOCALE).getString());
					webVO.setWebsiteID(websiteNode.getProperty(CCAdminConstant.WEBSITE_ID).getString());
					webVO.setWebsiteURL(websiteNode.getProperty(CCAdminConstant.WEBSITE_URL).getString());
					webVO.setWebsiteNodePath(websiteNode.getPath());
					websiteVOList.add(webVO);

				}
				return websiteVOList;
			}
		} catch (PathNotFoundException e) {
			log.error("Path not found", e);
		} catch (RepositoryException e) {
			log.error("Repository exception", e);
		}
		return null;
	}

	
	/**
	 * This will delete a website and all its associations from groups , given a list of website ids .
	 * @param websiteIds
	 * @param session
	 */
	@Override
	public void deleteWebsites(List<String> websiteIds , Session session) {

		try {

			for (String websiteId : websiteIds) {
				
				String websitePath = CCAdminConstant.WEBSITE_ROOT_NODE + websiteId ;
				if (session.nodeExists(websitePath)) {
					deleteWebsiteRefFromGroup(websitePath, session , websiteId);
					// TODO:: Need to comment it out .
					
					 session.getNode(websitePath).remove();
					 session.save();
					 
				}

			}
		} catch (RepositoryException e) {
			e.printStackTrace();
		} finally {
			session.logout();
		}

	}

	/**
	 * This method will delete website reference from all the groups , whenever
	 * a website is deleted.
	 * 
	 * @param websitePath
	 * @param session
	 */
	private void deleteWebsiteRefFromGroup(String websitePath, Session session, String websiteId) {
		
		System.out.println("website path in deletewebsiterom grp " + websitePath);
		try {

			Node websiteNode = session.getNode(websitePath);
			if (websiteNode.getProperty("webGroups").isMultiple()) {
				System.out.println("Webgroup property is a multivalued one");
				Value[] webGroupsValues = websiteNode.getProperty("webGroups").getValues();

				System.out.println("web group values size" + webGroupsValues.length);

				for (int i = 0; i < webGroupsValues.length; i++) {

					System.out.println("Inside forloop of multivalued");

					String groupNodeName = webGroupsValues[i].getString();
					if (session.nodeExists("/content/ccmessages/groups/" + groupNodeName)) {
						Node groupNode = session.getNode("/content/ccmessages/groups/" + groupNodeName);
						if (groupNode.hasProperty("websiteIds")) {

							Value[] websiteIDvalues = groupNode.getProperty("websiteIds").getValues();
							List<String> websiteIDList = new ArrayList<String>();
							// Add the websitePaths where the websitePath does
							// not match with the one deleted .
							for (Value websiteIDValue : websiteIDvalues) {
								if (!(websiteIDValue.getString()).equals(websiteId)) {
									websiteIDList.add(websiteIDValue.getString());
								}

							}

							String[] websiteIDArray = new String[websiteIDList.size()];
							int j = 0;

							for (String strWebsiteId : websiteIDList) {
								websiteIDArray[j] = strWebsiteId;
							}

							String grpNodePath = groupNode.getPath();
							groupNode.setProperty("websiteIds", websiteIDArray);

							session.save();
						}

					}

				}

			} else {

				Value webGroupsValue = websiteNode.getProperty("webGroups").getValue();
			}

		} catch (Exception e) {

		}

	}

	@Override
	public List<WebsiteVO> getSites(FilterVO filterVO) {
		return null;
	}

}
