package com.kelloggs.global.ccadmin.service.impl;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.jcr.AccessDeniedException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.jcr.resource.JcrResourceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kelloggs.global.ccadmin.constant.CCAdminConstant;
import com.kelloggs.global.ccadmin.service.AlertMessageService;
import com.kelloggs.global.ccadmin.ui.AlertMessageAction;
import com.kelloggs.global.ccadmin.util.CCAdminUtil;
import com.kelloggs.global.ccadmin.vo.AlertMessageVO;

/**
 * 
 * @author smukh5
 * 
 */
public class AlertMessageServiceImpl implements AlertMessageService {

	private static final Logger log = LoggerFactory.getLogger(AlertMessageServiceImpl.class);

	public static final String CQ_PAGE = "cq:Page";
	public static final String CQ_PAGE_CONTENT = "cq:PageContent";
	public static final String JCR_CONTENT = "jcr:content";
	public static final String KEY_CQ_DESIGN_PATH = "cq:designPath";
	public static final String KEY_CQ_TEMPLATE = "cq:template";
	public static final String KEY_PAGE_TITLE = "pageTitle";
	public static final String KEY_SLING_RESOURCE_TYPE = "sling:resourceType";
	public static final String PAGENODE_PATH_ETC_DESIGN = "/etc/designs/kelloggs/";
	public static final String CC_PAGE_COMPONENT = "/apps/Kelloggs/Global/components/page/enhancedcc/ccpage";
	public static final String PAGE_TITLE = "pageTitle";
	public static final String DEFAULT_CSS_PATH = "/etc/designs/kelloggs/Global/css/ccmessage/default_ccmessage.css";
	public static final String CC_MESSAGE_NODE = "ccmessage";
	public static final String NODE_TYPE_NT_UNSTRUCTURED = "nt:unstructured";
	public static final String CC_CONTENT_COMPONENT = "/apps/Kelloggs/Global/components/content/CriticalCommunicationV2/ccmessage";
	public static final String MESSAGE = "message";
	public static final String CSS_PATH = "cssPath";
	public static final String DETAIL_PAGE_PATH = "detailPagePath";
	public static final String ACTIVATE_STATUS = "activateStatus";
	public static final String IMAGE_PATH = "imagePath";

	@Override
	public String createAlertMessage(List<String> alertMessagesList, List<String> detailURLList, String imagePath,
			List<String> websitePaths, boolean doActivate, Session session) {

		AlertMessageVO alertMessageVO = new AlertMessageVO();
		alertMessageVO.setAlertMessages(alertMessagesList);
		alertMessageVO.setDetailMsgURL(detailURLList);
		alertMessageVO.setImagePath(imagePath);
		alertMessageVO.setWebsitePaths(websitePaths);

		CCAdminUtil ccAdminUtil = new CCAdminUtil();
		String nodeValue = ccAdminUtil.getNextValueForNode(CCAdminConstant.WEBSITE_TEMPLATE_ROOT_NODE, session);
		alertMessageVO.setAlertMessageID("alertMessage_" + nodeValue);
		String status = "";
		status = createAlertMessageInCRX(alertMessageVO, doActivate, session);
		return status;
	}

	private String createAlertMessageInCRX(AlertMessageVO alertMessageVO, boolean doActivate, Session session) {

		try {

			if (session.nodeExists("/content/ccmessages/alertMessages")) {
				Node alertMsgRootNode = session.getNode("/content/ccmessages/alertMessages");

				String alertMsgNodeName = alertMessageVO.getAlertMessageID();
				log.info("New Alert Msg Node Name " + alertMsgNodeName);

				log.info("Alert root node path is " + alertMsgRootNode.getPath());

				if (session.nodeExists(alertMsgRootNode.getPath() + "/" + alertMsgNodeName)) {

					return "Alert Message Exist";

				} else {

					Node currentAlertMsgNode = alertMsgRootNode.addNode(alertMsgNodeName);
					currentAlertMsgNode.setProperty(CCAdminConstant.ALERT_MESSAGE_IMAGE, alertMessageVO.getImagePath());
					currentAlertMsgNode.setProperty(CCAdminConstant.ALERT_MESSAGE_ID, alertMessageVO.getAlertMessageID());
					Iterator<String> websitePathItr = alertMessageVO.getWebsitePaths().iterator();
					Iterator<String> alertMessageItr = alertMessageVO.getAlertMessages().iterator();
					Iterator<String> detailURLItr = alertMessageVO.getDetailMsgURL().iterator();

					String[] websitePathArray = new String[alertMessageVO.getWebsitePaths().size()];

					String[] alertMessageArray = new String[alertMessageVO.getAlertMessages().size()];

					String[] detailURLArray = new String[alertMessageVO.getDetailMsgURL().size()];

					int i = 0;
					int j = 0;
					int k = 0;

					while (websitePathItr.hasNext()) {

						websitePathArray[i] = (String) websitePathItr.next();
						i++;
					}

					while (alertMessageItr.hasNext()) {

						alertMessageArray[j] = (String) alertMessageItr.next();
						j++;
					}

					while (detailURLItr.hasNext()) {

						detailURLArray[k] = (String) detailURLItr.next();
						k++;
					}

					currentAlertMsgNode.setProperty(CCAdminConstant.ALERT_MESSAGE_ASSOC_WEBSITEIDS, websitePathArray);
					currentAlertMsgNode.setProperty(CCAdminConstant.ALERT_MESSAGE, alertMessageArray);
					currentAlertMsgNode.setProperty(CCAdminConstant.ALERT_DETAIL_URL, detailURLArray);

					// TODO : Once the alert message has been saved , create the
					// ccpage with websitename and locale and activate it.
					// For each websitepath in goto CRX and create page with the
					// list of alert messages and imageurl and detail page path
					for (String websitePath : alertMessageVO.getWebsitePaths()) {

						createCCPage(websitePath, alertMessageVO.getAlertMessages(), alertMessageVO.getDetailMsgURL(),
								alertMessageVO.getImagePath(), session);

					}

					session.save();
					session.logout();

					return ("Created Alert Message");

				}
			} else {

				return "Alert Message Config Node does not exist";
			}

		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		} finally {
			session.logout();
		}

		return null;

	}

	/**
	 * Create CC pages and create the component and fill all the values inside
	 * it.
	 * 
	 * @param websitePath
	 * @param alertMessages
	 * @param detailMsgURL
	 * @param imagePath
	 */
	private void createCCPage(String websitePath, List<String> alertMessages, List<String> detailMsgURL, String imagePath, Session session) {

		Node parentNode = null;

		String rootNodePath = "";
		rootNodePath = "/content/ccmessages/ccPages";

		try {

			Calendar cal2 = new GregorianCalendar();
			long currentTime = cal2.getTimeInMillis();

			parentNode = session.getNode(rootNodePath);
			String nodeType = CQ_PAGE;
			/** Type of node to be created **/
			String relativePath = websitePath;
			parentNode = JcrResourceUtil.createPath(parentNode, relativePath, null, nodeType, true);

			Node ccMessageJCRNode = JcrResourceUtil.createPath(parentNode, JCR_CONTENT, null, CQ_PAGE_CONTENT, true);

			ccMessageJCRNode.setProperty(KEY_SLING_RESOURCE_TYPE, CC_PAGE_COMPONENT);
			ccMessageJCRNode.setProperty(PAGE_TITLE, new Long(currentTime).toString());
			ccMessageJCRNode.setProperty("jcr:title", "");
			ccMessageJCRNode.setProperty("cq:designPath", DEFAULT_CSS_PATH);
			if (ccMessageJCRNode != null) {
				Node ccDetailNode = JcrResourceUtil.createPath(ccMessageJCRNode, CC_MESSAGE_NODE, null, NODE_TYPE_NT_UNSTRUCTURED, true);
				ccDetailNode.setProperty(KEY_SLING_RESOURCE_TYPE, CC_CONTENT_COMPONENT);

				String[] alertMgsgArray = new String[alertMessages.size()];
				int i = 0;

				for (String alertMsg : alertMessages) {
					alertMgsgArray[i] = alertMsg;
				}

				String[] detailURLArray = new String[detailMsgURL.size()];
				int j = 0;

				for (String detailURL : detailMsgURL) {
					detailURLArray[i] = detailURL;
				}

				ccDetailNode.setProperty(MESSAGE, alertMgsgArray);
				ccDetailNode.setProperty(CSS_PATH, "");
				ccDetailNode.setProperty(DETAIL_PAGE_PATH, detailURLArray);
				ccDetailNode.setProperty(ACTIVATE_STATUS, "");
				ccDetailNode.setProperty(IMAGE_PATH, imagePath);

			}

		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteAlertMessage(String alertMessageName, Session session) {

		String alertMsgRootPath = "/content/ccmessages/alertMessages";
		String alertMsgPathToDelete = alertMsgRootPath + "/" + alertMessageName;

		try {
			session.getNode(alertMsgPathToDelete).remove();
			session.save();
			session.logout();

		} catch (AccessDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (VersionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.logout();
			}
		}

	}

	@Override
	public List<AlertMessageVO> getAllAlertMessages() {
		// TODO Auto-generated method stub
		return null;
	}

}
