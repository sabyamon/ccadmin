package com.kelloggs.global.ccadmin.service;

import java.util.List;
import javax.jcr.Session;

import com.kelloggs.global.ccadmin.vo.AlertMessageTempVO;

public interface MessageTemplateService {
	/**
	 * Creates a template in CRX , given a template name , an alert message and
	 * a detail url.
	 * 
	 * @param templateName
	 * @param alertMsg
	 * @param detailURL
	 * @param currentSession
	 * @return
	 */
	public String addTemplate(String templateName, String alertMsg,
			String detailURL, Session currentSession);

	/**
	 * Lists all templates created in CRX.
	 * 
	 * @param currentSession
	 * @return
	 */
	public List<AlertMessageTempVO> getAllTemplates(Session currentSession);

	/**
	 * Delete template given its name.
	 * 
	 * @param templateName
	 */
	public void deleteTemplate(String templateName, Session session);
	
	public String updateTemplate(AlertMessageTempVO alertMessageTempVO, Session currentSession);

}
