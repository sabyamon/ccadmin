package com.kelloggs.global.ccadmin.service;

import java.util.List;

import javax.jcr.Session;

import com.kelloggs.global.ccadmin.vo.AlertMessageVO;

public interface AlertMessageService {

	/**
	 * Creates an alert message and an alertmessage page in CRX given a list of
	 * alert messages , list of detail urls , an alert image and a list of
	 * websites .
	 * @param currentSession 
	 * 
	 * @return
	 */
	String createAlertMessage(List<String> alertMessagesList,
			List<String> detailURLList, String imagePath,
			List<String> websitePaths, boolean doActivate, Session currentSession);

	/**
	 * Deletes an alert message , given an alertmessage name and deactivates the
	 * alertmessage pages associated with this alert message .
	 * 
	 * @param alertMessageName
	 */
	public void deleteAlertMessage(String alertMessageName, Session session);

	/**
	 * This will list out all the alert messages and their associated websites .
	 * 
	 * @return
	 */
	public List<AlertMessageVO> getAllAlertMessages();

	

}
