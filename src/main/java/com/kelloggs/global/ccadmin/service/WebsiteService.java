package com.kelloggs.global.ccadmin.service;

import java.util.List;
import javax.jcr.Session;

import com.kelloggs.global.ccadmin.vo.FilterVO;
import com.kelloggs.global.ccadmin.vo.WebsiteVO;


public interface WebsiteService {

	/**
	 * Adds a website to CRX given a website URL, and its locale.
	 * @param websiteURL
	 * @param locale
	 * @param currentSession
	 * @return
	 */
	public WebsiteVO addWebsite(String websiteURL , String locale , Session currentSession);
	
	/**
	 * Fetches all the websites from CRX .
	 * @param currentSession
	 * @return
	 */
	public List<WebsiteVO> getAllWebsites(Session currentSession);
	
	/**
	 * Delete websites given their nodes
	 * The method will also delete the references of the deleted website in the webGroups and alerts
	 * @param websitePaths
	 */
	public void deleteWebsites(List<String> websitePaths,Session session);
	
	/**
	 * Return a list of websites that have been filtered according to the criteria
	 * listed in the filterVO
	 */
	public List<WebsiteVO> getSites(FilterVO filterVO);
	
}
