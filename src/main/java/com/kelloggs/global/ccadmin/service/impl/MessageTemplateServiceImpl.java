package com.kelloggs.global.ccadmin.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.AccessDeniedException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.kelloggs.global.ccadmin.constant.CCAdminConstant.*;

import com.day.cq.replication.Replicator;
import com.kelloggs.global.ccadmin.constant.CCAdminConstant;
import com.kelloggs.global.ccadmin.constant.WebsiteEnum;
import com.kelloggs.global.ccadmin.service.MessageTemplateService;
import com.kelloggs.global.ccadmin.util.CCAdminUtil;
import com.kelloggs.global.ccadmin.vo.AlertMessageTempVO;
import com.kelloggs.global.ccadmin.vo.GroupVO;

/**
 * This implementation class deals with all sorts of interactions with CRX for
 * Managing alert template.
 * 
 * @author sabya
 * 
 */
public class MessageTemplateServiceImpl implements MessageTemplateService {

	private static final Logger log = LoggerFactory.getLogger(MessageTemplateServiceImpl.class);
	private Replicator replicator;

	public MessageTemplateServiceImpl() {

	}

	public MessageTemplateServiceImpl(SlingHttpServletRequest slingRequest, Replicator pReplicator) {
		replicator = pReplicator;
	}

	@Override
	public String addTemplate(String templateName, String alertMsg, String detailURL, Session currentSession) {

		log.info("Inside ManageWebsiteImpl : addWebsite method");
		AlertMessageTempVO templateVO = new AlertMessageTempVO();
		// templateVO.setAlertTempID(templateName + Math.random());
		CCAdminUtil ccAdminUtil = new CCAdminUtil();
		String nodeValue = ccAdminUtil.getNextValueForNode(CCAdminConstant.WEBSITE_TEMPLATE_ROOT_NODE, currentSession);
		templateVO.setAlertTempID("template_" + nodeValue);
		templateVO.setAlertMessage(alertMsg);
		templateVO.setDetailURL(detailURL);
		templateVO.setTemplateName(templateName);

		String status = createTemplateInCRX(templateVO, currentSession);

		return status;

	}

	/**
	 * This method creates a template in CRX given an AlertMessageTemplateVO.
	 * 
	 * @param templateVO
	 * @param currentSession
	 * @return
	 */
	private String createTemplateInCRX(AlertMessageTempVO templateVO, Session currentSession) {
		log.info("Creating template node in createTemplateInCRX()");
		try {
			if (currentSession.nodeExists(WEBSITE_TEMPLATE_ROOT_NODE)) {
				Node templateRootNode = currentSession.getNode(WEBSITE_TEMPLATE_ROOT_NODE);
				String templateNodeName = templateVO.getAlertTempID();
				log.info("new template name " + templateNodeName);
				log.info("template root node path is " + templateRootNode.getPath());

				if (currentSession.nodeExists(templateRootNode.getPath() + "/" + templateNodeName)) {
					return "Template Exists";
				} else {
					Node currentWebsiteNode = templateRootNode.addNode(templateNodeName);
					currentWebsiteNode.setProperty(WebsiteEnum.templateId.name(), templateVO.getAlertTempID());
					currentWebsiteNode.setProperty(WebsiteEnum.templateName.name(), templateVO.getTemplateName());
					currentWebsiteNode.setProperty(WebsiteEnum.detailPagePath.name(), templateVO.getDetailURL());
					currentWebsiteNode.setProperty(WebsiteEnum.alertMsg.name(), templateVO.getAlertMessage());

					currentSession.save();
					currentSession.logout();

					return ("Create Template " + templateNodeName);

				}
			} else {

				return "Template Config Node Does not exist";
			}

		} catch (PathNotFoundException e) {
			log.error("No Path", e);
		} catch (RepositoryException e) {
			log.error("Repository problem", e);
		} finally {
			currentSession.logout();
		}
		return null;

	}

	/**
	 * Implementation method for getting all templates created before .
	 * 
	 * @param Session
	 * @return List<AlertMessageTempVO>
	 */
	@Override
	public List<AlertMessageTempVO> getAllTemplates(Session currentSession) {

		List<AlertMessageTempVO> templateVOList = new ArrayList<AlertMessageTempVO>();

		try {
			if (currentSession.nodeExists(CCAdminConstant.WEBSITE_TEMPLATE_ROOT_NODE)) {
				Node templateRootNode = currentSession.getNode(CCAdminConstant.WEBSITE_TEMPLATE_ROOT_NODE);

				NodeIterator ndItr = templateRootNode.getNodes();

				while (ndItr.hasNext()) {

					AlertMessageTempVO templateVO = new AlertMessageTempVO();
					Node templateNode = ndItr.nextNode();
					if (templateNode.hasProperty(CCAdminConstant.TEMPLATE_NAME)) {
						templateVO.setTemplateName(templateNode.getProperty(CCAdminConstant.TEMPLATE_NAME).getString());
					}

					if (templateNode.hasProperty(CCAdminConstant.TEMPLATE_ALERT_MESSAGE)) {
						templateVO.setAlertMessage(templateNode.getProperty(CCAdminConstant.TEMPLATE_ALERT_MESSAGE).getString());
					}

					if (templateNode.hasProperty(CCAdminConstant.TEMPLATE_DETAIL_PAGE_PATH)) {
						templateVO.setDetailURL(templateNode.getProperty(CCAdminConstant.TEMPLATE_DETAIL_PAGE_PATH).getString());
					}

					if (templateNode.hasProperty(CCAdminConstant.WEBSITE_ID)) {
						templateVO.setDetailURL(templateNode.getProperty(CCAdminConstant.WEBSITE_ID).getString());
					}

					templateVOList.add(templateVO);

				}

			} else {
				log.info("Template config Node does not exist");
				System.out.println("Template config Node does not exist");
			}
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return templateVOList;

	}

	/**
	 * This method will delete a template , given a template name and a jcr
	 * session.
	 */
	@Override
	public void deleteTemplate(String templateNodeName, Session session) {

		String templatePathToDelete = CCAdminConstant.WEBSITE_TEMPLATE_ROOT_NODE + templateNodeName;

		try {
			session.getNode(templatePathToDelete).remove();
			session.save();
			session.logout();

		} catch (AccessDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (VersionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.logout();
			}
		}

	}

	@Override
	public String updateTemplate(AlertMessageTempVO alertMessageTempVO, Session session) {
		try {
			if (session.nodeExists(WEBSITE_TEMPLATE_ROOT_NODE + alertMessageTempVO.getTemplateName())) {
				Node currentWebsiteNode = session.getNode(WEBSITE_TEMPLATE_ROOT_NODE + alertMessageTempVO.getTemplateName());
				String propertyName = currentWebsiteNode.getProperty("templateName").getString();
				System.out.println("Node exists " + propertyName);
				currentWebsiteNode.setProperty("templateName", "Testing1");
				session.save();
			}
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
