package com.kelloggs.global.ccadmin.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jcr.AccessDeniedException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Replicator;
import com.kelloggs.global.ccadmin.constant.CCAdminConstant;
import com.kelloggs.global.ccadmin.service.GroupService;
import com.kelloggs.global.ccadmin.util.CCAdminUtil;
import com.kelloggs.global.ccadmin.vo.FilterVO;
import com.kelloggs.global.ccadmin.vo.GroupVO;

/**
 * This is the implementation class which deals with all sorts of actions
 * performed in CRX.
 * 
 * @author sabya
 * 
 */

public class GroupServiceImpl implements GroupService {

	private static final Logger log = LoggerFactory.getLogger(GroupServiceImpl.class);
	private Replicator replicator;

	/**
	 * Default Constructor for testing
	 */
	public GroupServiceImpl() {

	}

	public GroupServiceImpl(SlingHttpServletRequest slingRequest, Replicator pReplicator) {
		replicator = pReplicator;
	}

	/**
	 * This method will add new groups to the list of groups and will replicate
	 * it.
	 */
	public String addGroup(String groupName, List<String> websiteNodeNames, Session currentSession) {

		String status = "";

		GroupVO groupVO = new GroupVO();

		CCAdminUtil ccAdminUtil = new CCAdminUtil();
		String nodeValue = ccAdminUtil.getNextValueForNode(CCAdminConstant.WEBSITE_GROUPS_ROOT_NODE, currentSession);
		groupVO.setGroupID("group_" + nodeValue);
		groupVO.setGroupName(groupName);
		groupVO.setWebsiteNodeNames(websiteNodeNames);
		status = createGroupInCRX(currentSession, groupVO);

		return status;

	}

	/**
	 * This method will create group in crx and add properties to the group node
	 * like "group name" , website names and their paths associated with this
	 * group.
	 * 
	 * @param currentSession
	 * @param groupVO
	 * @return
	 */
	private String createGroupInCRX(Session currentSession, GroupVO groupVO) {

		log.info("Creating group node in createGroupInCRX()");
		try {

			if (currentSession.nodeExists(CCAdminConstant.WEBSITE_GROUPS_ROOT_NODE)) {
				Node groupRootNode = currentSession.getNode(CCAdminConstant.WEBSITE_GROUPS_ROOT_NODE);

				String groupNodeName = groupVO.getGroupID();
				log.info("new group node name " + groupNodeName);
				log.info("Group root node path is " + groupRootNode.getPath());

				if (currentSession.nodeExists(groupRootNode.getPath() + "/" + groupNodeName)) {

					return "Group Exists";

				} else {

					Node currentGroupNode = groupRootNode.addNode(groupNodeName);
					currentGroupNode.setProperty(CCAdminConstant.GROUP_ID, groupVO.getGroupID());
					currentGroupNode.setProperty(CCAdminConstant.GROUP_NAME, groupVO.getGroupName());

					Iterator<String> nameItr = groupVO.getWebsiteNodeNames().iterator();
					String[] websiteNameArray = new String[groupVO.getWebsiteNodeNames().size()];

					int j = 0;

					while (nameItr.hasNext()) {

						websiteNameArray[j] = (String) nameItr.next();
						j++;
					}

					currentGroupNode.setProperty(CCAdminConstant.GROUP_WEBSITE_IDS, websiteNameArray);
					currentSession.save();
					// return ("Create Group " + groupNodeName);

					// Save the group name for each and every website that
					// belong to this group.

					for (String websiteName : groupVO.getWebsiteNodeNames()) {
						associateGroupNameInWebsites(websiteName, currentSession, groupVO.getGroupID());
					}

				}
			} else {

				return "Group Config Node Does not exist";
			}

		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		} finally {
			currentSession.logout();
		}

		return "";
	}

	/**
	 * This method will associate groupnames with websites. Websites will
	 * contain the group names to which they belong to.
	 * 
	 * @param websiteNodename
	 * @param currentSession
	 * @param groupName
	 */
	private void associateGroupNameInWebsites(String websiteNodename, Session session, String groupName) {

		Node websiteNode;
		String websiteNodePath = CCAdminConstant.WEBSITE_ROOT_NODE + websiteNodename;
		// String[] groupNameProperty = {groupName} ;

		try {
			if (session.nodeExists(websiteNodePath)) {
				websiteNode = session.getNode(websiteNodePath);
				if (websiteNode.hasProperty(CCAdminConstant.WEBSITE_ASSOC_GROUP)) {
					// Get the property values and append the new value .
					if (websiteNode.getProperty(CCAdminConstant.WEBSITE_ASSOC_GROUP).isMultiple()) {
						Value[] groupValues = websiteNode.getProperty(CCAdminConstant.WEBSITE_ASSOC_GROUP).getValues();
						List<String> newGrouppathsList = new ArrayList<String>();

						for (Value websiteValue : groupValues) {
							newGrouppathsList.add(websiteValue.getString());
						}

						newGrouppathsList.add(groupName);

						String[] groupNamesArray = new String[newGrouppathsList.size()];

						int i = 0;
						for (String strGroupName : newGrouppathsList) {
							groupNamesArray[i] = strGroupName;
							i++;
						}

						websiteNode.setProperty(CCAdminConstant.WEBSITE_ASSOC_GROUP, groupNamesArray);
						session.save();

					}

				}

			}
		} catch (RepositoryException e) {
			log.error("Repository Exception :: " +  e.getMessage());
		}

	}

	/**
	 * This method will get all the groups created before and send it back to
	 * the caller client.
	 * 
	 * @return List<GroupVO>
	 * @param Session
	 *            currentSession
	 */
	public List<GroupVO> getAllGroups(Session currentSession) {

		List<GroupVO> groupVOList = new ArrayList<GroupVO>();

		try {

			if (currentSession.nodeExists(CCAdminConstant.WEBSITE_GROUPS_ROOT_NODE)) {
				Node groupRootNode = currentSession.getNode(CCAdminConstant.WEBSITE_GROUPS_ROOT_NODE);
				NodeIterator ndItr = groupRootNode.getNodes();

				while (ndItr.hasNext()) {

					GroupVO groupVO = new GroupVO();
					Node groupNode = ndItr.nextNode();
					if (groupNode.hasProperty("groupId")) {
						groupVO.setGroupID(groupNode.getProperty("groupId").getString());
					}

					if (groupNode.hasProperty("groupName")) {
						groupVO.setGroupName(groupNode.getProperty("groupName").getString());
					}

					// Get all the websitenames associated with the group
					if (groupNode.hasProperty("websiteIds")) {
						if (groupNode.getProperty("websiteIds").isMultiple()) {

							List<String> websiteNamesList = new ArrayList<String>();
							Value[] websiteNameValues = groupNode.getProperty("websiteIds").getValues();

							for (Value websiteNameValue : websiteNameValues) {
								websiteNamesList.add(websiteNameValue.getString());
							}
							groupVO.setWebsiteNodeNames(websiteNamesList);

						} else {

							List<String> websiteNameList = new ArrayList<String>();
							websiteNameList.add(groupNode.getProperty("websiteIds").getString());
							groupVO.setWebsiteNodeNames(websiteNameList);

						}
					} else {
						// No website associated with this group. Hence set
						// empty list to the groupVO.
						List<String> websiteNameList = new ArrayList<String>();
						websiteNameList.add("");
						groupVO.setWebsiteNodeNames(websiteNameList);
					}

					groupVOList.add(groupVO);

				}

			} else {
				log.info("Group config Node does not exist");
				System.out.println("Group config Node does not exist");
			}
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return groupVOList;

	}

	/**
	 * This method deletes group given a list of group names .
	 */
	@Override
	public void deleteGroup(List<String> groupNameList, Session session) {

		for (String groupName : groupNameList) {
			String groupPathToDelete = CCAdminConstant.WEBSITE_GROUPS_ROOT_NODE + groupName;

			try {

				if (session.nodeExists(groupPathToDelete)) {
					Node groupNode = session.getNode(groupPathToDelete);
					// 5. Upon deletion of a group , delete group name from all
					// websites
					
					//removeRefOfGroupFromWebsites(groupNode, session);
					
					deleteGroupRefFromWebsite(groupPathToDelete,session,groupName);

					session.getNode(groupPathToDelete).remove();
					session.save();
					session.logout();
				}

			} catch (AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (VersionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LockException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ConstraintViolationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PathNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (session != null) {
					session.logout();
				}
			}

		}

	}

	/**
	 * 
	 * @param groupNode
	 * @param session
	 * @throws RepositoryException
	 * @throws PathNotFoundException
	 * @throws ValueFormatException
	 */
	private void removeRefOfGroupFromWebsites(Node groupNode, Session session) throws ValueFormatException, PathNotFoundException,
			RepositoryException {

		String groupName = "";
		groupName = groupNode.getName();

		if (groupNode.hasProperty("websitePaths")) {

			if (groupNode.getProperty("websitePaths").isMultiple()) {

				Value[] websitePathsValueArr = groupNode.getProperty("websitePaths").getValues();
				List<String> websitePathsList = new ArrayList<String>();

				int i = 0;
				for (Value websitegrp : websitePathsValueArr) {
					websitePathsList.add(websitePathsValueArr[i].getString());
					i++;
				}

				// Now i have a list of website paths.
				// Access each and every website node and get the webGroup
				// property and remove the group from that and set it again.

				for (String websitePath : websitePathsList) {

					removeGroupRefFromWebsiteNode(groupName, websitePath, session);

				}

			}

		}

	}

	/*
	 * private void deleteGroupFromWebsiteNode(String websitePath, String
	 * groupName, Session session) throws RepositoryException {
	 * 
	 * Node websiteNode = session.getNode(websitePath);
	 * if(websiteNode.hasProperty("webGroups")){
	 * 
	 * Value[]
	 * 
	 * 
	 * }
	 * 
	 * 
	 * }
	 */

	/**
	 * Given a groupname , a list of website paths and a list of website names ,
	 * this method will delete the websites from the groups and delete the group
	 * name from the websites as well.
	 */
	@Override
	public void deleteWebsiteFromGroup(String groupName, String websiteName, String websitePath, Session session) {

		// websitePath = /content/ccmessages/websites/www_pintrest_com_en_US
		// websiteName = www_pintrest_com_en_US
		// groupName = North_America_ES

		/**
		 * TODO :: Delete the websitepath from group node , websitePaths
		 * property /content/ccmessages/groups/North_America_ES Delete groupName
		 * from the webGroups property of the website deleted.
		 */

		log.info("Website to be deleted " + websiteName + " with path " + websitePath);
		log.info("Website to bve deleted from group " + groupName);

		try {

			if (session.nodeExists("/content/ccmessages/groups")) {

				if (session.nodeExists("/content/ccmessages/groups/" + groupName)) {
					Node groupNode = session.getNode("/content/ccmessages/groups/" + groupName);
					if (groupNode.hasProperty("websitePaths")) {
						Value[] websitePathsValueArr = groupNode.getProperty("websitePaths").getValues();
						List<String> websitePathList = new ArrayList<String>();
						for (Value websitePathValue : websitePathsValueArr) {
							if (!(websitePathValue.getString()).equals(websitePath)) {
								websitePathList.add(websitePathValue.getString());
							}

						}

						// Now websitePathList will contain every website paths
						// except the one deleted.

						String[] websitePathArr = new String[websitePathList.size()];
						int i = 0;
						for (String strWebsitePath : websitePathList) {
							websitePathArr[i] = strWebsitePath;
							i++;
						}
						groupNode.setProperty("websitePaths", websitePathArr);
						session.save();

						// Now website path is removed from group node.
						// Now remove the groupname from the website as well.

						removeGroupRefFromWebsiteNode(groupName, websitePath, session);

					} else {
						log.info("website paths does not exist");
					}
				} else {
					log.info("Website rootnode does not exist");
				}

			} else {
				log.info("Group Config does not exit");
			}

		} catch (RepositoryException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param groupName
	 * @param websitePath
	 * @param session
	 * @throws RepositoryException
	 */
	private void removeGroupRefFromWebsiteNode(String groupName, String websitePath, Session session) throws RepositoryException {

		if (session.nodeExists(websitePath)) {
			Node websiteNode = session.getNode(websitePath);
			if (websiteNode.hasProperty("webGroups")) {

				if (websiteNode.getProperty("webGroups").isMultiple()) {

					Value[] webgroupValueArr = websiteNode.getProperty("webGroups").getValues();
					List<String> websiteGroupFiltererdList = new ArrayList<String>();

					for (Value websitegrp : webgroupValueArr) {
						if (!(websitegrp.getString()).equals(groupName)) {

							websiteGroupFiltererdList.add(websitegrp.getString());
						}

					}

					String[] webGroupFilteredArr = new String[websiteGroupFiltererdList.size()];

					int i = 0;
					for (String strGroupName : websiteGroupFiltererdList) {
						webGroupFilteredArr[i] = strGroupName;
						i++;
					}
					websiteNode.setProperty("webGroups", webGroupFilteredArr);
					session.save();

				} else {
					Value websiteGroupValue = websiteNode.getProperty("webGroups").getValue();
					// TODO:: Need to do it for single valued property .
				}

			}
		}

	}

	/**
	 * Will be implemented later. If groups can not be filtered in client side,
	 * then only this method will be implemented.
	 */
	@Override
	public List<GroupVO> getGroups(FilterVO filterVO) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
private void deleteGroupRefFromWebsite(String groupPath, Session session, String groupID) {
		
		System.out.println("website path in deletewebsiterom grp " + groupPath);
		try {

			Node grpNode = session.getNode(groupPath);
			if (grpNode.getProperty("websiteIds").isMultiple()) {
				System.out.println("Webgroup property is a multivalued one");
				Value[] websiteIDValues = grpNode.getProperty("websiteIds").getValues();

				System.out.println("web id values size" + websiteIDValues.length);

				for (int i = 0; i < websiteIDValues.length; i++) {

					System.out.println("Inside forloop of multivalued");

					String websiteNodeName = websiteIDValues[i].getString();
					if (session.nodeExists("/content/ccmessages/websites/" + websiteNodeName)) {
						Node websiteNode = session.getNode("/content/ccmessages/websites/" + websiteNodeName);
						if (websiteNode.hasProperty("webGroups")) {

							Value[] groupIDValues = websiteNode.getProperty("webGroups").getValues();
							List<String> groupIDList = new ArrayList<String>();
							// Add the websitePaths where the websitePath does
							// not match with the one deleted .
							for (Value groupIDValue : groupIDValues) {
								if (!(groupIDValue.getString()).equals(groupID)) {
									groupIDList.add(groupIDValue.getString());
								}

							}

							String[] groupIDArray = new String[groupIDList.size()];
							int j = 0;

							for (String strGroupID : groupIDList) {
								groupIDArray[j] = strGroupID;
								j++ ;
							}

							//String grpNodePath = websiteNode.getPath();
							websiteNode.setProperty("webGroups", groupIDArray);
							session.save();
						}

					}

				}

			} else {

				Value webGroupsValue = grpNode.getProperty("webGroups").getValue();
			}

		} catch (Exception e) {

		}

	}

}
