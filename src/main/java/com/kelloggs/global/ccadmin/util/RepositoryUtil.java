package com.kelloggs.global.ccadmin.util;


import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import org.apache.jackrabbit.commons.JcrUtils;


/**
 * Utility class for managing JCR repositories. <b>Note</b>: most of the utility
 * methods in this class can be used only with Jackrabbit.
 * 
 */
public class RepositoryUtil {

   /** Item path separator */
   public static final String PATH_SEPARATOR = "/";


   public static Repository getRepository(String url) {
      // Get the Repository object
	   Repository repository = null;
	  try{ 
	       repository = JcrUtils.getRepository(url);
	      
	  }catch(Exception e){
		 e.printStackTrace();
      }
	  return repository;
   }

   /**
    * Connect to a JCR repository
    * 
    * @param repository
    *           The JCR repository
    * @param user
    *           The user name
    * @param password
    *           The password
    * @return a valid JCR session
    * 
    * @throws RepositoryException
    *            when it is not possible to connect to the JCR repository
    */

   public static Session login(String url,String userName,String pwd,String workspace)  {
      try {
         Session session = getRepository(url).login(
               new SimpleCredentials(userName, pwd.toCharArray()),workspace);
         return session;
      } catch (Exception e) {
         e.printStackTrace();
      }
      return null;

   }

 
}
