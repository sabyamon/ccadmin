package com.kelloggs.global.ccadmin.util;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import com.kelloggs.global.ccadmin.constant.CCAdminConstant;

public class CCAdminUtil {

	public String getNextValueForNode(String nodePath, Session currentSession) {

		try {

			Node node = currentSession.getNode(nodePath);
			Long nextValue = 0L;

			synchronized (this) {
				nextValue = node.getProperty(CCAdminConstant.SEQUENCE_NUMBER).getLong();
				node.setProperty(CCAdminConstant.SEQUENCE_NUMBER, nextValue + 1);
			}

			currentSession.save();
			return String.format("%d", nextValue);

		} catch (PathNotFoundException pathNotFoundException) {

		} catch (RepositoryException repositoryException) {

		}

		return "";

	}

}
