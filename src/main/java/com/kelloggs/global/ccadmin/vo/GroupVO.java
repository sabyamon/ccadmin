package com.kelloggs.global.ccadmin.vo;

import java.util.List;

/**
 * This VO will represent a Group which will contain a groupname , a group id ,
 * a List of website paths and a List of websitenames associated with this group
 * .
 * 
 * @author sabya
 * 
 */

public class GroupVO {

	String groupID;
	String groupName;
	List<String> websiteNodeNames;
	
	public String getGroupID() {
		return groupID;
	}
	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public List<String> getWebsiteNodeNames() {
		return websiteNodeNames;
	}
	public void setWebsiteNodeNames(List<String> websiteNodeNames) {
		this.websiteNodeNames = websiteNodeNames;
	}
	
}
