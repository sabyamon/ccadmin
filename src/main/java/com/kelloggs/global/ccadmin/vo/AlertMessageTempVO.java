package com.kelloggs.global.ccadmin.vo;

public class AlertMessageTempVO {

	
	/**
	 * This VO will represent an Alert Message Template . A Template can have one template name , an alert message and a detail url and an ID. 
	 */
	
	String alertTempID ;
	String templateName ;
	String alertMessage ;
	String detailURL ;
	
	public String getAlertTempID() {
		return alertTempID;
	}
	public void setAlertTempID(String alertTempID) {
		this.alertTempID = alertTempID;
	}
	
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getAlertMessage() {
		return alertMessage;
	}
	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}
	public String getDetailURL() {
		return detailURL;
	}
	public void setDetailURL(String detailURL) {
		this.detailURL = detailURL;
	}
	
	
	
}
