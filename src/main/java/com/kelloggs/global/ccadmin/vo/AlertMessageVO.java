package com.kelloggs.global.ccadmin.vo;

import java.util.List;

public class AlertMessageVO {

	List<String> alertMessages;
	List<String> detailMsgURL;
	String imagePath;
	List<String> websitePaths;
	String alertMessageID ;

	public String getAlertMessageID() {
		return alertMessageID;
	}

	public void setAlertMessageID(String alertMessageID) {
		this.alertMessageID = alertMessageID;
	}

	public List<String> getAlertMessages() {
		return alertMessages;
	}

	public void setAlertMessages(List<String> alertMessages) {
		this.alertMessages = alertMessages;
	}

	public List<String> getDetailMsgURL() {
		return detailMsgURL;
	}

	public void setDetailMsgURL(List<String> detailMsgURL) {
		this.detailMsgURL = detailMsgURL;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public List<String> getWebsitePaths() {
		return websitePaths;
	}

	public void setWebsitePaths(List<String> websitePaths) {
		this.websitePaths = websitePaths;
	}

}
