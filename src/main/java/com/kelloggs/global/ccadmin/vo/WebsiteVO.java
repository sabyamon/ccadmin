package com.kelloggs.global.ccadmin.vo;

/**
 * This VO will represent a Node which has the property websiteID , website url and locale associated with a website url.
 * @author sabya
 *
 */

public class WebsiteVO {

	String websiteID ;
	String websiteURL ;
	String locale ;
	String websiteNodePath ;
	
	public String getWebsiteNodePath() {
		return websiteNodePath;
	}
	public void setWebsiteNodePath(String websiteNodePath) {
		this.websiteNodePath = websiteNodePath;
	}
	public String getWebsiteID() {
		return websiteID;
	}
	public void setWebsiteID(String websiteID) {
		this.websiteID = websiteID;
	}
	public String getWebsiteURL() {
		return websiteURL;
	}
	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	
}
