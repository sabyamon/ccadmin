package com.kelloggs.global.ccadmin.vo;

import java.util.List;

/**
 * Captures the filter criteria that needs to be applied to the sites
 * @author speru1
 *
 */
public class FilterVO {

	private String status;
	private List<String> countries;
	private List<String> languages;
}
