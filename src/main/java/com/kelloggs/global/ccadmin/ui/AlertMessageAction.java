package com.kelloggs.global.ccadmin.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.replication.Replicator;
import com.kelloggs.global.ccadmin.constant.AdminConstantsEnum;
import com.kelloggs.global.ccadmin.service.AlertMessageService;
import com.kelloggs.global.ccadmin.service.impl.AlertMessageServiceImpl;

/**
 * This action class will take the request from client and delegate according to
 * the action.
 * 
 * @author smukh5
 * 
 */

@Component(immediate = true, metatype = false, label = "CC Admin V3 Manage Alert Message Entry Point")
@Service(value = javax.servlet.Servlet.class)
@Properties({ @Property(name = "sling.servlet.paths", value = "/bin/ccadminmanagemessage.json") })
public class AlertMessageAction extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 42L;
	private static final Logger log = LoggerFactory
			.getLogger(AlertMessageAction.class);

	@Reference
	protected Replicator replicator;

	@Reference
	private SlingRepository repository;

	@Override
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		handleRequest(request, response);
	}

	@Override
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		handleRequest(request, response);

	}

	/**
	 * Extract data from SlingHTTPRequest and perform according to that.
	 * 
	 * @param request
	 * @param response
	 */
	private void handleRequest(SlingHttpServletRequest request,
			SlingHttpServletResponse response) {

		log.info("Inside handleRequest() method of ManageAlertMessageAction ");

		String action = "";

		if (request != null
				&& request.getParameter(AdminConstantsEnum.ACTION.toString()) != null) {

			action = request.getParameter(AdminConstantsEnum.ACTION.toString());
		}

		Session currentSession = getCRXSession();

		if (action.equalsIgnoreCase("createAlertMessage")) {

			// Create Alert message .
			String alertMessages = "";
			String detailURLs = "";
			String imagePath = "";
			String websitePaths = "";

			alertMessages = (request.getParameter("alertMessages") != null) ? (request
					.getParameter("alertMessages")) : "";
			detailURLs = (request.getParameter("detailURLs") != null) ? (request
					.getParameter("detailURLs")) : "";
			websitePaths = (request.getParameter("websitePaths") != null) ? (request
					.getParameter("websitePaths")) : "";
			List<String> alertMessagesList = new ArrayList<String>();
			List<String> detailURLList = new ArrayList<String>();
			List<String> websitePathsList = new ArrayList<String>();

			for (String alertMsg : alertMessages.split(",")) {
				alertMessagesList.add(alertMsg);
			}

			for (String detailURL : detailURLs.split(",")) {
				detailURLList.add(detailURL);
			}

			for (String websitePath : websitePaths.split(",")) {
				websitePathsList.add(websitePath);
			}

			String status = "";
			AlertMessageService manageAlertMessageService = new AlertMessageServiceImpl();
			status = manageAlertMessageService.createAlertMessage(
					alertMessagesList, detailURLList, imagePath,
					websitePathsList , true, currentSession);

		} else if (action.equalsIgnoreCase("getAllMessage")) {
			// Do Something
		} else {
			
		}

	}

	/**
	 * This method gives back the admin session to create node . TODO:: Need to
	 * make it an utility methid and move it to the utility class .
	 * 
	 * @return
	 */
	private Session getCRXSession() {

		Session currentSession = null;
		try {
			currentSession = repository.loginAdministrative(null);
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return currentSession;
	}

}
