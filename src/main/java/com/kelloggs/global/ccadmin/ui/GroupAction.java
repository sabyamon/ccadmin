package com.kelloggs.global.ccadmin.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.replication.Replicator;
import com.kelloggs.global.ccadmin.service.GroupService;
import com.kelloggs.global.ccadmin.service.impl.GroupServiceImpl;
import com.kelloggs.global.ccadmin.vo.GroupVO;

@Component(immediate = true, metatype = false, label = "CC Admin V3 Manage Group Entry Point")
@Service(value = javax.servlet.Servlet.class)
@Properties({ @Property(name = "sling.servlet.paths", value = "/bin/ccadminmanagegroup.json") })
/**
 * This service will deal with all manage group methods .
 * @author smukh5
 *
 */
public class GroupAction extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(GroupAction.class);

	@Reference
	protected Replicator replicator;

	@Reference
	private SlingRepository repository;

	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws javax.servlet.ServletException,
			java.io.IOException {
		handleRequest(request, response);
	}

	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws javax.servlet.ServletException,
			java.io.IOException {
		handleRequest(request, response);
	}

	// This method is going to deal with all kinds of requests related to manage
	// website.
	private void handleRequest(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {

		response.setContentType("application/json");
		log.info("Inside Handle Request in ManageGroupAction");
		String action = "";

		if (request != null && request.getParameter("action") != null) {
			action = request.getParameter("action");
		}

		Session currentSession = getCRXSession();

		/**
		 * This block will deal with creating groups.
		 */

		/**
		 * TODO :: Null Check for all the request params .
		 */

		if (action.equalsIgnoreCase("createGroup")) {

			String status = "";
			String groupName = (request.getParameter("groupName") != null) ? request.getParameter("groupName") : "";
			String websitePaths = (request.getParameter("websitePaths") != null) ? request.getParameter("websitePaths") : "";
			String websiteNames = (request.getParameter("websiteNames") != null) ? request.getParameter("websiteNames") : "";

			List<String> websitePathList = new ArrayList<String>();
			List<String> websiteNameList = new ArrayList<String>();

			for (String websitePath : websitePaths.split(",")) {
				websitePathList.add(websitePath);
			}

			for (String websiteName : websiteNames.split(",")) {
				websiteNameList.add(websiteName);
			}

			log.info("website path list is " + websitePathList);
			log.info("website name list is " + websiteNameList);

			GroupService manageGroupInterfaces = new GroupServiceImpl(request, replicator);
			status = manageGroupInterfaces.addGroup(groupName, websitePathList, currentSession);

			TidyJSONWriter writer = new TidyJSONWriter(response.getWriter());
			writer.setTidy(true);

			createJSONResponse(status, writer);

		} else if (action.equalsIgnoreCase("getGroupList")) {

			GroupService manageGroupInterfaces = new GroupServiceImpl(request, replicator);
			List<GroupVO> groupVOList = manageGroupInterfaces.getAllGroups(currentSession);

			TidyJSONWriter writer = new TidyJSONWriter(response.getWriter());
			writer.setTidy(true);
			try {

				writer.array();
				for (GroupVO groupVO : groupVOList) {
					writer.object();
					writer.key("text").value("groupName");
					writer.key("value").value(groupVO.getGroupName());
					writer.key("text").value("groupID");
					writer.key("value").value(groupVO.getGroupID());
					writer.key("text").value("websitePaths");
					writer.key("value").value(groupVO.getWebsiteNodeNames());
					writer.endObject();
				}

				writer.endArray();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			TidyJSONWriter writer = new TidyJSONWriter(response.getWriter());
			writer.setTidy(true);
			try {
				writer.object();
				writer.key("text").value("status");
				writer.key("value").value("Action is not valid");
				writer.endObject();
				writer.endArray();
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

	}

	private void createJSONResponse(String status, TidyJSONWriter writer) {
		try {
			writer.array();
			writer.object();
			writer.key("text").value("addstatus");
			writer.key("value").value(status);
			writer.endObject();
			writer.endArray();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Session getCRXSession() {

		Session currentSession = null;
		try {
			currentSession = repository.loginAdministrative(null);
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return currentSession;
	}

}
