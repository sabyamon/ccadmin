package com.kelloggs.global.ccadmin.ui;

import java.io.IOException;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.replication.Replicator;
import com.kelloggs.global.ccadmin.constant.AdminConstantsEnum;
import com.kelloggs.global.ccadmin.service.MessageTemplateService;
import com.kelloggs.global.ccadmin.service.WebsiteService;
import com.kelloggs.global.ccadmin.service.impl.MessageTemplateServiceImpl;
import com.kelloggs.global.ccadmin.service.impl.WebsiteServiceImpl;
import com.kelloggs.global.ccadmin.vo.WebsiteVO;

@Component(immediate = true, metatype = false, label = "CC Admin V3 Manage Alert Template Entry Point")
@Service(value = javax.servlet.Servlet.class)
@Properties({ @Property(name = "sling.servlet.paths", value = "/bin/ccadminmanagetemplate.json") })
/**
 * This service will deal with all manage alert template methods .
 * @author smukh5
 *
 */
public class AlertTemplateAction extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory
			.getLogger(AlertTemplateAction.class);

	@Reference
	protected Replicator replicator;

	@Reference
	private SlingRepository repository;

	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response)
			throws javax.servlet.ServletException, java.io.IOException {
		handleRequest(request, response);
	}

	// This method is going to deal with all kinds of requests related to manage
	// website.
	private void handleRequest(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {

		response.setContentType("application/json");
		log.info("Inside Handle Request in ManageAlertTemplateAction");
		String action = "";
		
		if(request !=null && request.getParameter(AdminConstantsEnum.ACTION
				.toString()) != null){
			action = request.getParameter(AdminConstantsEnum.ACTION
					.toString());
			
		}

		Session currentSession = getCRXSession();

		/**
		 * This block will deal with creating templates .
		 */
		if (action.equalsIgnoreCase("createTemplate")) {

			String templateName = (request.getParameter("templateName") != null) ? request.getParameter("templateName") : "" ;
			String alertMsg = (request.getParameter("alertMsg") != null) ? request.getParameter("alertMsg") : "" ;
			String detailURL = (request.getParameter("detailURL") != null) ? request.getParameter("detailURL") : "" ;
			
			MessageTemplateService manageTemplateService = new MessageTemplateServiceImpl(
					request, replicator);
			String status = manageTemplateService.addTemplate(templateName, alertMsg, detailURL, currentSession);

			TidyJSONWriter writer = new TidyJSONWriter(response.getWriter());
			writer.setTidy(true);

			createJSONResponse(status, writer);

		} else if (action.equalsIgnoreCase("getAllTemplates")) {

			WebsiteService manageWebsiteInterfaces = new WebsiteServiceImpl(
					request, replicator);
			List<WebsiteVO> websiteVOList = manageWebsiteInterfaces
					.getAllWebsites(currentSession);

			TidyJSONWriter writer = new TidyJSONWriter(response.getWriter());
			writer.setTidy(true);
			try {

				writer.array();
				for (WebsiteVO websiteVO : websiteVOList) {
					writer.object();
					writer.key("text").value("wesbiteURL");
					writer.key("value").value(websiteVO.getWebsiteURL());
					writer.key("text").value("websiteID");
					writer.key("value").value(websiteVO.getWebsiteID());
					writer.key("text").value("websiteLocale");
					writer.key("value").value(websiteVO.getLocale());
					writer.key("text").value("websiteNodePath");
					writer.key("value").value(websiteVO.getWebsiteNodePath());
					writer.endObject();
				}

				writer.endArray();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}else{
			TidyJSONWriter writer = new TidyJSONWriter(response.getWriter());
			writer.setTidy(true);
			try {
				writer.object();
				writer.key("text").value("status");
				writer.key("value").value("Action is not valid");
				writer.endObject();
				writer.endArray();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}

	private void createJSONResponse(String status, TidyJSONWriter writer) {
		try {
			writer.array();
			writer.object();
			writer.key("text").value("addstatus");
			writer.key("value").value(status);
			writer.endObject();
			writer.endArray();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Session getCRXSession() {

		Session currentSession = null;
		try {
			currentSession = repository.loginAdministrative(null);
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return currentSession;
	}

	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response)
			throws javax.servlet.ServletException, java.io.IOException {
		handleRequest(request, response);
	}

}
