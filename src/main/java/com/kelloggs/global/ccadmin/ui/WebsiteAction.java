package com.kelloggs.global.ccadmin.ui;

import java.io.IOException;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.replication.Replicator;
import com.kelloggs.global.ccadmin.constant.AdminConstantsEnum;
import com.kelloggs.global.ccadmin.service.WebsiteService;
import com.kelloggs.global.ccadmin.service.impl.WebsiteServiceImpl;
import com.kelloggs.global.ccadmin.vo.WebsiteVO;

@Component(immediate = true, metatype = false, label = "CC Admin V3 Manage Website Entry Point")
@Service(value = javax.servlet.Servlet.class)
@Properties({ @Property(name = "sling.servlet.paths", value = "/bin/ccadminmanagewebsite.json") })
/**
 * This service will deal with all manage website methods .
 * @author smukh5
 *
 */
public class WebsiteAction extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory
			.getLogger(WebsiteAction.class);

	@Reference
	protected Replicator replicator;

	@Reference
	private SlingRepository repository;

	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response)
			throws javax.servlet.ServletException, java.io.IOException {
		handleRequest(request, response);
	}

	// This method is going to deal with all kinds of requests related to manage website.
	private void handleRequest(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {

		response.setContentType("application/json");
		log.info("Inside Handle Request in CCAdmin Service");
		String action = request.getParameter(AdminConstantsEnum.ACTION.toString());

		Session currentSession = getCRXSession();

		/**
		 * This block will deal with creating websites .
		 */
		if (action.equalsIgnoreCase("createWebsite")) {

			String websiteURL = request.getParameter("weburl");
			String locale = request.getParameter("locale");
			WebsiteService manageWebsiteInterfaces = new WebsiteServiceImpl(
					request, replicator);
			String status = manageWebsiteInterfaces.addWebsite(websiteURL,
					locale, currentSession).getWebsiteNodePath();

			TidyJSONWriter writer = new TidyJSONWriter(response.getWriter());
			writer.setTidy(true);
			
			createJSONResponse(status, writer);

		} else if (action.equalsIgnoreCase("getWebsiteList")) {

			WebsiteService manageWebsiteInterfaces = new WebsiteServiceImpl(
					request, replicator);
			List<WebsiteVO> websiteVOList = manageWebsiteInterfaces
					.getAllWebsites(currentSession);

			TidyJSONWriter writer = new TidyJSONWriter(response.getWriter());
			writer.setTidy(true);
			try {
				
				writer.array();
				for (WebsiteVO websiteVO : websiteVOList) {
					writer.object();
					writer.key("text").value("wesbiteURL");
					writer.key("value").value(websiteVO.getWebsiteURL());
					writer.key("text").value("websiteID");
					writer.key("value").value(websiteVO.getWebsiteID());
					writer.key("text").value("websiteLocale");
					writer.key("value").value(websiteVO.getLocale());
					writer.key("text").value("websiteNodePath");
					writer.key("value").value(websiteVO.getWebsiteNodePath());
					writer.endObject();
				}
				
				writer.endArray();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}

	private void createJSONResponse(String status, TidyJSONWriter writer) {
		try {
			writer.array();
			writer.object();
			writer.key("text").value("addstatus");
			writer.key("value").value(status);
			writer.endObject();
			writer.endArray();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Session getCRXSession() {

		Session currentSession = null;
		try {
			currentSession = repository.loginAdministrative(null);
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return currentSession;
	}

	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response)
			throws javax.servlet.ServletException, java.io.IOException {
		handleRequest(request, response);
	}

	

}
