package com.kelloggs.global.ccadmin.constant;

public class CCAdminConstant {

	public final static String WEBSITE_TEMPLATE_ROOT_NODE = "/content/ccmessages/templates/";
	public final static String WEBSITE_ROOT_NODE = "/content/ccmessages/websites/";
	public final static String WEBSITE_GROUPS_ROOT_NODE = "/content/ccmessages/groups/";
	public final static String SEQUENCE_NUMBER = "sequnceNumber";
	public final static String WEBSITE_ID_PREFIX = "website_";

	// Group node properties

	public final static String GROUP_WEBSITE_IDS = "websiteIds";
	public final static String GROUP_ID = "groupId";
	public final static String GROUP_NAME = "groupName";

	// Template Node properties

	public static final String TEMPLATE_ID = "templateId";
	public static final String TEMPLATE_NAME = "templateName";
	public static final String TEMPLATE_ALERT_MESSAGE = "alertMsg";
	public static final String TEMPLATE_DETAIL_PAGE_PATH = "detailPagePath";

	// Website Node Propeties

	public static final String WEBSITE_ID = "websiteId";
	public static final String WEBSITE_LOCALE = "websiteLocale";
	public static final String WEBSITE_URL = "websiteURL";
	public static final String WEBSITE_ASSOC_GROUP = "webGroups";
	public static final String WEBSITE_ASSOC_ALERTS = "webAlerts";

	// Alert Message Node Properties

	public static final String ALERT_MESSAGE_ID = "alertMessageID";
	public static final String ALERT_MESSAGE = "alertMessage";
	public static final String ALERT_MESSAGE_IMAGE = "alertMsgImage";
	public static final String ALERT_DETAIL_URL = "detailURL";
	public static final String ALERT_MESSAGE_ASSOC_WEBSITEIDS = "websitePaths";

}
