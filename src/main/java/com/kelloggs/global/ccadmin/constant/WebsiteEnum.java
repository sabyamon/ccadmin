package com.kelloggs.global.ccadmin.constant;

public enum WebsiteEnum {

	WEBSITE_ID_PREFIX{
		public String toString(){
			return "website_";
		}
	},
	WEBSITES_NODE{
		public String toString(){
			return "/content/ccmessages/websites";
		}
	},
	GROUP_ROOT_NODE{
		public String toString(){
			return "/content/ccmessages/groups";
		}
	},
	TEMPLATE_ROOT_NODE{
		public String toString(){
			return "/content/ccmessages/templates";
		}
	},
	//NODE PROPERTIES
	websiteId,websiteURL,websiteLocale,webGroups,webAlerts,
	groupId,groupName,websitePaths,websiteNames,
	templateId,templateName,detailPagePath,alertMsg
	
}
