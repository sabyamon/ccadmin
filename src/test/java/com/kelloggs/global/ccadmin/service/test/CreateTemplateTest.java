package com.kelloggs.global.ccadmin.service.test;

import com.kelloggs.global.ccadmin.service.MessageTemplateService;
import com.kelloggs.global.ccadmin.service.impl.MessageTemplateServiceImpl;

public class CreateTemplateTest extends BaseTest{

	/**
	 * Method to test the create template functionality .
	 */
	public void testCreateTemplate(){
		MessageTemplateService service = new MessageTemplateServiceImpl();
		String templateName = "Site Down";
		String alertMsg = "This website will be down for next 2 hours";
		String detailURL = "www.specialk.com";
		
		/*List<String> websitePaths = new ArrayList<String>();
		List<String> websiteNames = new ArrayList<String>();*/
		
		String status = service.addTemplate(templateName, alertMsg, detailURL, session);
		
		try {
			//assertEquals("Create Template " + templateName ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}
	
}
