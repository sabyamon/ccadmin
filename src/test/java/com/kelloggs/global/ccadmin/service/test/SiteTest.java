package com.kelloggs.global.ccadmin.service.test;

import javax.jcr.Node;

import com.kelloggs.global.ccadmin.constant.WebsiteEnum;
import com.kelloggs.global.ccadmin.service.WebsiteService;
import com.kelloggs.global.ccadmin.service.impl.WebsiteServiceImpl;
import com.kelloggs.global.ccadmin.vo.WebsiteVO;

public class SiteTest extends BaseTest{

	/**
	 * Method to test the create website functionality .
	 */
	public void testCreateSite(){
		WebsiteService service = new WebsiteServiceImpl();
		String siteUrl = "www.testsite1.com";
		WebsiteVO vo = service.addWebsite(siteUrl, "en_US", session);
		
		//ok the node should be created
		try {
			Node node = session.getNode(vo.getWebsiteNodePath());
			assertEquals(siteUrl,node.getProperties(WebsiteEnum.websiteURL.name()));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}
	
	/**
	 * Method to test the create group functionality .
	 */
	/*
	public void testCreateGroup(){
		ManageGroupService service = new ManageGroupServiceImpl();
		String groupName = "North_America_EN";
		List<String> websitePaths = new ArrayList<String>();
		List<String> websiteNames = new ArrayList<String>();
		
		String status = service.addGroup(groupName, websitePaths, websiteNames, session);
		
		try {
			assertEquals("Create Group " + groupName ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}*/
}
