package com.kelloggs.global.ccadmin.service.test;

import javax.jcr.Session;

import com.kelloggs.global.ccadmin.util.RepositoryUtil;

import junit.framework.TestCase;

/** 
 * Base test that contains the generic setup and teardown 
 * 
 * @author speru1
 *
 */
public class BaseTest extends TestCase{

	protected Session session = null;
	
	@Override
	public void setUp(){
		String url = "http://localhost:7502/crx/server";
		String userName = "admin";
		String pwd = "admin";
		String wks = "crx.default";
		
		session = RepositoryUtil.login(url, userName, pwd, wks); 
	}
	
	@Override
	public void tearDown(){
		session.logout();
	}
}
