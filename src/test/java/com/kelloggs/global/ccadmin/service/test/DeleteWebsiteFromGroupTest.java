package com.kelloggs.global.ccadmin.service.test;

import com.kelloggs.global.ccadmin.service.GroupService;
import com.kelloggs.global.ccadmin.service.impl.GroupServiceImpl;

public class DeleteWebsiteFromGroupTest extends BaseTest{
	
	/**
	 * Method to test the create template functionality .
	 */
	public void testDeleteWebsite(){
		
		String websiteNameToDelete = "www_pintrest_com_en_US" ;
		String websitepathToDelete = "/content/ccmessages/websites/www_pintrest_com_en_US" ;
		String groupName = "North_America_ES" ;
		
		GroupService service = new GroupServiceImpl();
		service.deleteWebsiteFromGroup(groupName, websiteNameToDelete, websitepathToDelete, session);
		
		try {
			//assertEquals("Create Template " + templateName ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}

}
