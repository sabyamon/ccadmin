package com.kelloggs.global.ccadmin.service.test;

import java.util.List;

import com.kelloggs.global.ccadmin.service.WebsiteService;
import com.kelloggs.global.ccadmin.service.impl.WebsiteServiceImpl;
import com.kelloggs.global.ccadmin.vo.WebsiteVO;

public class CreateWebsiteTest extends BaseTest {

	/**
	 * Method to test the create group functionality .
	 */
	public void testCreateWebsite() {
		WebsiteService service = new WebsiteServiceImpl();
		String websiteURL = "www.chompa.in";
		String locale = "en_US";
		WebsiteVO websiteVO = service.addWebsite(websiteURL, locale, session);

		//System.out.println(websiteVO.getWebsiteID());
		try {
			// assertEquals("Create Group " + groupName ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}

	}

	public void testGetAllWebsites() {

		WebsiteService service = new WebsiteServiceImpl();
		List<WebsiteVO> websiteVOList = service.getAllWebsites(session);

		for(WebsiteVO webVO : websiteVOList){
			System.out.println("website url: " + webVO.getWebsiteURL());
			System.out.println("website locale: " + webVO.getWebsiteURL());
			System.out.println("website path : " + webVO.getWebsiteNodePath());
		}
		
		try {
			// assertEquals("Create Group " + groupName ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}

	}

}
