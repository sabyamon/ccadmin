package com.kelloggs.global.ccadmin.service.test;

import com.kelloggs.global.ccadmin.service.MessageTemplateService;
import com.kelloggs.global.ccadmin.service.impl.MessageTemplateServiceImpl;

public class TemplateTest extends BaseTest{

	/**
	 * Method to test the create template functionality .
	 */
	public void testCreateTemplate(){
		MessageTemplateService service = new MessageTemplateServiceImpl();
		String templateName = "Site_Down_ES";
		String alertMsg = "This site will be down for 3 hrs";
		String detailURL = "www.google.com";
		
		/*List<String> websitePaths = new ArrayList<String>();
		List<String> websiteNames = new ArrayList<String>();*/
		
		String status = service.addTemplate(templateName, alertMsg, detailURL, session);
		
		try {
			assertEquals("Create Template " + templateName ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}

}
