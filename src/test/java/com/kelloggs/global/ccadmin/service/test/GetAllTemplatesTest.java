package com.kelloggs.global.ccadmin.service.test;

import java.util.List;

import com.kelloggs.global.ccadmin.service.GroupService;
import com.kelloggs.global.ccadmin.service.MessageTemplateService;
import com.kelloggs.global.ccadmin.service.impl.GroupServiceImpl;
import com.kelloggs.global.ccadmin.service.impl.MessageTemplateServiceImpl;
import com.kelloggs.global.ccadmin.vo.AlertMessageTempVO;
import com.kelloggs.global.ccadmin.vo.GroupVO;

public class GetAllTemplatesTest extends BaseTest{
	
	
	/**
	 * Method to test the create group functionality .
	 */
	public void testGetAllTemplates(){
		MessageTemplateService service = new MessageTemplateServiceImpl();
				
		List<AlertMessageTempVO> templateVOList = service.getAllTemplates(session);
		
		for(AlertMessageTempVO templateVO : templateVOList){
			
			System.out.println("Template name is " + templateVO.getTemplateName());
			System.out.println("Alert Message is " + templateVO.getAlertMessage());
			System.out.println("Detail url is " + templateVO.getDetailURL());
			System.out.println("******************************");
			
		}
		
		try {
			//assertEquals("Create Group " + groupName ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}

}
