package com.kelloggs.global.ccadmin.service.test;

import java.util.ArrayList;
import java.util.List;

import com.kelloggs.global.ccadmin.service.AlertMessageService;
import com.kelloggs.global.ccadmin.service.impl.AlertMessageServiceImpl;

public class AlertMessageTest extends BaseTest{
	
	/**
	 * Method to test the alert message functionality .
	 */
	public void testCreateAlertMessage(){
		
		AlertMessageService service = new AlertMessageServiceImpl();
		List<String> alertMessages = new ArrayList<String>();
		List<String> websitePaths = new ArrayList<String>();
		List<String> detailURL = new ArrayList<String>();
		String imagePath = "/content/dam/ccadmin/image.gif" ;
		alertMessages.add("This is a test") ;
		alertMessages.add("This is another test") ;
		websitePaths.add("www_google_com") ;
		websitePaths.add("www_fadfish_com") ;
		detailURL.add("www.google.com");
		detailURL.add("www.fadfish.com/admin");
		
		String status = service.createAlertMessage(alertMessages, detailURL, imagePath, websitePaths, true, session);
		
		try {
			assertEquals("Created Alert Message"  ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}

}
