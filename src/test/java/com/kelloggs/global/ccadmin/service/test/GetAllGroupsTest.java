package com.kelloggs.global.ccadmin.service.test;

import java.util.List;

import com.kelloggs.global.ccadmin.service.GroupService;
import com.kelloggs.global.ccadmin.service.impl.GroupServiceImpl;
import com.kelloggs.global.ccadmin.vo.GroupVO;

public class GetAllGroupsTest extends BaseTest{
	
	
	/**
	 * Method to test the create group functionality .
	 */
	public void testGetAllGroups(){
		GroupService service = new GroupServiceImpl();
				
		List<GroupVO> groupVOList = service.getAllGroups(session);
		
		for(GroupVO grpVO : groupVOList){
			
			System.out.println("groupname is " + grpVO.getGroupName());
			System.out.println("group id is " + grpVO.getGroupID());
			System.out.println("website names are " + grpVO.getWebsiteNodeNames());
			
		}
		
		try {
			//assertEquals("Create Group " + groupName ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}

}
