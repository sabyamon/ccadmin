package com.kelloggs.global.ccadmin.service.test;

import org.junit.Test;

import com.kelloggs.global.ccadmin.service.MessageTemplateService;
import com.kelloggs.global.ccadmin.service.impl.MessageTemplateServiceImpl;
import com.kelloggs.global.ccadmin.vo.AlertMessageTempVO;

public class ManageTemplateServiceTest extends BaseTest {

	@Test
	public void testUpdateTemplate(){
		
		MessageTemplateService manageTemplateService = new MessageTemplateServiceImpl();
		
		AlertMessageTempVO alertMessageTempVO = new AlertMessageTempVO();
		
		alertMessageTempVO.setTemplateName("Site_Down_ES");
		
		manageTemplateService.updateTemplate(alertMessageTempVO, session);
		
		try {
			//assertEquals("Created Alert Message");
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}
	
	@Test
	public void testGetAllTemplates() {
		MessageTemplateService manageTemplateService = new MessageTemplateServiceImpl();
		manageTemplateService.getAllTemplates(session);
	}

}
