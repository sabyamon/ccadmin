package com.kelloggs.global.ccadmin.service.test;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Session;

import junit.framework.TestCase;

import com.kelloggs.global.ccadmin.constant.WebsiteEnum;
import com.kelloggs.global.ccadmin.service.GroupService;
import com.kelloggs.global.ccadmin.service.WebsiteService;
import com.kelloggs.global.ccadmin.service.impl.GroupServiceImpl;
import com.kelloggs.global.ccadmin.service.impl.WebsiteServiceImpl;
import com.kelloggs.global.ccadmin.util.RepositoryUtil;
import com.kelloggs.global.ccadmin.vo.WebsiteVO;

public class CreateGroupTest extends BaseTest{
	
	
	/**
	 * Method to test the create group functionality .
	 */
	public void testCreateGroup(){
		GroupService service = new GroupServiceImpl();
		
		String groupName = "South_America_ES";
		List<String> websiteNames = new ArrayList<String>();

		websiteNames.add("website_4");
		websiteNames.add("website_5");
		
		String status = service.addGroup(groupName,  websiteNames, session);
		
		try {
			//assertEquals("Create Group " + groupName ,status);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		} 
		
	}
	
	

}
